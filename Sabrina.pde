ArrayList<Witch> witches = new ArrayList<Witch>();

void setup()
{
  size(555, 555, P2D);
  background(128, 128, 255);
  fill(128, 42, 128, 64);
    
  witches.add(new Witch(new PVector(0,     vMid()),
                        new PVector(width, vMid())));

  witches.add(new Witch(new PVector(0,     vMid() / 2),
                        new PVector(width, vMid() / 2)));
                        
  witches.add(new Witch(new PVector(0,     vMid() + vMid() / 2),
                        new PVector(width, vMid() + vMid() / 2)));
                        
  witches.add(new Witch(new PVector(0,     vMid() / 4),
                        new PVector(width, vMid() / 4)));
                        
  witches.add(new Witch(new PVector(0,     vMid() + 3 * vMid() / 4),
                        new PVector(width, vMid() + 3 * vMid() / 4)));
  //witches.add(new Witch(new PVector(hMid(), 0),
  //                      new PVector(hMid(), height), 0, 0));

}

void draw()
{
  stroke(0);
  rect(0, 0, width, height);
  int cycleLength = 10000;
  float t = (millis() % cycleLength) * 2 * PI / cycleLength;

  for (int i = 0; i < witches.size(); ++i)
  {
    Witch w = witches.get(i);
    
    if (i == 0)
      w.setRune(rad(PI * sin(t) / 2,
                    PI * cos(t) / 2));
    else if (i <= 2)
      w.setRune(rad(PI * sin(t + i * 0.23) / 5,
                    PI * cos(t - i * 0.23) / 5));
    else
      w.setRune(rad(PI * sin(t + i * 0.23) / 9,
                    PI * cos(t - i * 0.23) / 9));

    for (int s = 3; s >= 1; --s)
    {
    stroke(255 - i * 64 / (4 - s),
           255 - 10 * s,
           255 - (s-1) * 100,
           s * 192 / 3);
    strokeWeight(s * 3);
    w.draw();
    }
  }
}

float vMid() { return height / 2.0; }
float hMid() { return  width / 2.0; }
float quarterMin() { return halfMin() / 2.0; }
float halfMin() { return min(width, height) / 2.0; }
