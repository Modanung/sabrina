class Witch
{
  PShape curve = new PShape(LINE);
  PVector[] broom = new PVector[2]; 
  ArrayList<Rune> wand = new ArrayList<Rune>();
  
  void draw()
  { 
    for (int i = 0; i < curve.getVertexCount() - 1; ++i)
      line(curve.getVertex(i).x,     curve.getVertex(i).y,
           curve.getVertex(i + 1).x, curve.getVertex(i + 1).y);
  }
  Witch(PVector from, PVector to, float in, float out)
  {
    broom[0] = from;
    broom[1] = to;
    wand.add(spoke(90, 0, 0, 0));
    
    burn();
  }
  
  Witch(PVector from, PVector to)
  {
    broom[0] = from;
    broom[1] = to;
    
    burn();
  }
    
  void setRune(Rune rune)
  {
    wand.clear();
    wand.add(rune);
    burn();
  }
    
  void burn()
  {
    curve = new PShape(LINE);
    curve.beginShape(LINE);
    curve.stroke(0, 255, 255);
    curve.vertex(from().x, from().y);
    
    if (length() == 0.0f)
    {
      curve.endShape();
      return;
    }

    int resolution = ceil(max(1, length() * 0.055f));
    
    if (wand.size() > 0)
    {
      //QPointF offset{};
      //float angle{};
  
      for (int w = 0; w < wand.size(); ++w)
      {
          Rune r = wand.get(w);
  
          //QMatrix branch{};
          //float s{ c.get(Crook::LENGTH, w) };
          //angle -= c.get(Crook::ANGLE, w);
  
          //branch.translate(offset.x(), offset.y());
          //branch.scale(s, s);
          //branch.rotate(angle);
  
          float dt = 1.0 / resolution;
  
          for (int i = 1; i <= resolution; ++i)
          {
            PVector last = tip();
            PVector pos = r.plot(i * dt).mult(length()).add(from());
            curve.vertex(pos.x, pos.y);
          }
  
          //offset += branch.map(QPointF{ 1.0f, 0.0f });
      }
    }
    else
    {    
      curve.vertex(to().x, to().y);
    }
    
    curve.endShape();
  }
  
  PVector from() { return broom[0]; }
  PVector   to() { return broom[1]; }
  float length() { return to().copy().sub(from()).mag(); }
  PVector  tip()
  {
    float x = curve.getVertexX(curve.getVertexCount() - 1);
    float y = curve.getVertexY(curve.getVertexCount() - 1);
    return new PVector(x, y);
  }
}

class Rune
{
  float[] r = new float[2];
  float[] p = new float[2];
  
  Rune(float r1, float r2, float p1, float p2)
  {
    r[0] = r1;
    r[1] = r2;
    p[0] = p1;
    p[1] = p2;
  }
  
  Rune(float r1, float r2, float p0)
  {
    r[0] = r1;
    r[1] = r2;
    p[0] = p0;
    p[1] = p0;
  }
  
  Rune(float r1, float r2)
  {
    r[0] = r1;
    r[1] = r2;
    p[0] = 0;
    p[1] = 0;
  }
  
  Rune()
  {
    r[0] = 0;
    r[1] = 0;
    p[0] = 0;
    p[1] = 0;
  }

  PVector lerped(float t)
  {
      return new PVector( lerp(r[0], r[1], t),
                          lerp(p[0], p[1], t));
  }

  PVector plot(float t)
  {
    float sinPiT = sin(PI * t);
    float cosPiT = cos(PI * t);
    PVector ap = lerped(t);
    float a = ap.x;
    float p = ap.y * 5.0f / PI;

    float x = lerp(t, (1.0 - cosPiT) / 2.0,
                   a * a * (1.0 - p));

    float y = lerp(0.0, sinPiT / 2.0,
                   a);

    return new PVector(x, -y);
  }
  
  void replace(Rune rune)
  {
    r[0] = rune.r[0];
    r[1] = rune.r[1];
    p[0] = rune.p[0];
    p[1] = rune.p[1];
  }
}

float lerpAngle(float a, float b, float max, float t)
{
    float aCycled = cycle(a, 0.0f, max);
    float bCycled = cycle(b, 0.0f, max);

    if (aCycled > bCycled)
    {
        float temp = aCycled;
        aCycled = bCycled; bCycled = temp;
    }

    if (bCycled - aCycled < aCycled - bCycled + max)
        return lerp(aCycled, bCycled, t);
    else
        return cycle(lerp(bCycled, aCycled + max, t), 0.0f, max);
}

Rune spoke(float in, float out, float p1, float p2)
{
    in  = cycle( in, -4.0f, 4.0f);
    out = cycle(out, -4.0f, 4.0f);

    float pinchIn  = 1.0f + 0.5f * p1;
    float pinchOut = 1.0f + 0.5f * p2;
    float r1 = min(1.0f, max(-1.0f, pinchIn  * in  / 2));
    float r2 = min(1.0f, max(-1.0f, pinchOut * out / 2));

    if (abs(in)  > 2)
        p1 -= abs(in)  - abs(r1) * 2;
    if (abs(out) > 2)
        p2 -= abs(out) - abs(r2) * 2;

    if (abs(in)  > 3)
    {
        float s = r1 == 0.0f ? r1 :
                               r1 / abs(r1);

        r1 -= s * (abs(in) - abs(p1) * 3) / 3;
        p1 += (abs(in) - abs(r1) - 2) / 3;
    }
    
    if (abs(out) > 3)
    {
        float s = r2 == 0.0f ? r2 :
                               r2 / abs(r2);

        r2 -= s * (abs(out) - abs(p2) * 3) / 3;
        p2 += (abs(out) - abs(r2) - 2) / 3;
    }

    return new Rune(r1, r2, p1, p2);
}

Rune spoke(float in, float out) {  return spoke(in, out, 0, 0); }
Rune   rad(float in, float out, float p1, float p2) {  return spoke(4 * in / PI, 4 * out / PI, p1, p2); }
Rune   rad(float in, float out) {  return rad(in, out, 0, 0); }

float cycle(float x, float min, float max)
{
    float res = x;

    if (min > max)
    {
        float temp = min;
        min = max; max = temp;
    }
    
    float range = max - min;

    if (x < min)
        res += range * ceil((min - x) / range);
    else if (x > max)
        res -= range * ceil((x - max) / range);

    return res;
}
